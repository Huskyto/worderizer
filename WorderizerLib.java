
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;


/** Worderizer Library
 * Converts a String text into a character drawn version and prints it
 * to the console or to a file.
 *
 * @author Huskyto Gonar Khan
 * @version 1.01 
 * @since 2009/11/23
 */

class WorderizerLib {
	
    private String[] letters = new String[140];  //letras en caracteres
    private String[] alpha = new String[28];  //listado de letras
    private int[] positions = new int[28];  //listado de posiciones
	
    /** Constructor
     * Calls the data loaders "loadLetters()" and "loadPositions()".
     */
        
    public WorderizerLib() {
    	loadLetters();
    	loadPositions();
    }
    
    /**
     * Loads the 'drawn' letters from the file "data.let" and stores
     * them on the "letters" array.
     */
    
    private void loadLetters() {  
    	try{
            BufferedReader loader = new BufferedReader(new FileReader("data.let"));
            String tmp=loader.readLine();
            for(int i=0;i<this.letters.length;i++){
                this.letters[i]=tmp;
                tmp=loader.readLine();
            }
        }catch(IOException ioe){}
    }
    
    /**
     * Reads the reference list of letters, as well as their positions on the
     * "letters" array, from the file: "pos.let".
     * Stores the letter 's references on the "alpha" array, and their 
     * relative position on the "positions" array.
     */
    
    private void loadPositions(){
    	try{
            BufferedReader loader = new BufferedReader(new FileReader("pos.let"));
            String tmp = loader.readLine();
            int nul;
            for(int i = 0; i < this.positions.length; i++){
                StringTokenizer tok = new StringTokenizer(tmp);
                this.alpha[i] = tok.nextToken("#");
                nul=Integer.parseInt(tok.nextToken("#"));
                this.positions[i] = nul;
                tmp=loader.readLine();
            }
        }catch(IOException ioe){}
    }
    
    /** Debug Only
     * Prints the Letters array.
     */
    
    public void printletters(){
    	for(int i = 0; i < 140; i++) System.out.println(this.letters[i]);
    }
    
    /** Debug Only
     * Prints the Alpha array.
     */
    
    public void printalpha(){
    	for(int i = 0; i < 28; i++) System.out.println(this.alpha[i]);
    }
    
    /** Debug Only
     * Prints the Positions array.
     */
    
    public void printpositions(){
    	for(int i = 0; i < 28; i++) System.out.println(this.positions[i]);
    }
    
    /**
     * Takes a String and transforms it to drawn characters that then 
     * prints into the console.
     * 
     * @param s String to be converted.
     */
    
    public void printer(String s){
    	int[] writers=new int[s.length()];
    	for(int i = 0; i < s.length(); i++){
            for(int j = 0; j < 28; j++){
            	if(Character.toUpperCase(s.charAt(i)) == this.alpha[j].charAt(0)){
                    writers[i] = this.positions[j];
                    break;
    		}
            }
    	}    	
    	for(int i = 0; i < 5; i++){  //imprime el arreglo
            for(int j = 0; j < s.length(); j++)
                System.out.print(letters[(writers[j] + i - 1)]);
            System.out.println();
    	}
    }
    
    /**
     * Takes a String and transforms it to drawn characters, then it's
     * printed into a given file.
     * 
     * @param s String to be converted.
     * @param f The name of the output file.
     */
    
    public void printer(String s, String f){
        try{
            PrintWriter output = new PrintWriter(new FileWriter(f + ".txt"));
            int[] writers = new int[s.length()];
            for(int i=0;i<s.length();i++){
                for(int j=0;j<28;j++){
                    if(Character.toUpperCase(s.charAt(i)) == this.alpha[j].charAt(0)){
                        writers[i] = this.positions[j];
                        break;
                    }else writers[i] = 0;
                }
            }
            for(int i = 0; i < 5; i++){
                for(int j = 0;j < s.length(); j++){
                    if(writers[j] != 0) output.print(letters[(writers[j] + i - 1)]);
                }
                output.println();
            }
            output.close();
        }catch(IOException ioe) {}
    }
    
    /**
     * Changes the draw characters.
     * 
     * @param ca Background character.
     * @param cb Foreground character.
     */
    
    public void change(char ca,char cb){
    	char c1 = this.letters[0].charAt(0);
    	char c2 = this.letters[0].charAt(2);
    	String stmp = "";
    	for(int i = 0; i < 140; i++){
            for(int j = 0; j < this.letters[i].length(); j++){
                if(this.letters[i].charAt(j) == c1) stmp += ca;
    		if(this.letters[i].charAt(j) == c2) stmp += cb;
            }
            this.letters[i] = stmp;
            stmp = "";
    	}
    }
    
}